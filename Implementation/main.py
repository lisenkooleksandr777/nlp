import time
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from gensim import utils
from gensim.models.callbacks import CallbackAny2Vec
from gensim.models import Word2Vec
from sklearn.decomposition import PCA
from matplotlib import pyplot

class GofTSentencesCorpus:
    """Data reader/preparator, for now just read it as is"""

    def __iter__(self):
        file_path = '../Data/g_of_t.txt'
        for line in open(file_path):
            # assume there's one document per line, tokens separated by whitespace
            result = utils.simple_preprocess(line)
            #print(result)
            yield  result

class HPSentencesCorpus:
    """Data reader/preparator, for now just read it as is"""

    def __iter__(self):
        file_path = '../Data/hp.txt'
        for line in open(file_path):
            # assume there's one document per line, tokens separated by whitespace
            result = utils.simple_preprocess(line)
            #print(result)
            yield  result

# init callback class
class Callback(CallbackAny2Vec):
    """
    Callback to print loss after each epoch
    """
    def __init__(self):
        self.epoch = 0

    def on_epoch_end(self, model):
        loss = model.get_latest_training_loss()
        
        if self.epoch == 0:
            print('Loss after epoch {}: {}'.format(self.epoch, loss))
        elif self.epoch % 50 == 0:
            print('Loss after epoch {}: {}'.format(self.epoch, loss- self.loss_previous_step))
        
        self.epoch += 1
        self.loss_previous_step = loss

def visualizeModel(modelName, wordsNumber):
    model = Word2Vec.load(modelName)

    X = model[model.wv.vocab]
    pca = PCA(n_components=2)
    result = pca.fit_transform(X)
    pyplot.scatter(result[:wordsNumber, 0], result[:wordsNumber, 1])
    words = list(model.wv.vocab)

    th = wordsNumber + 1
    for i, word in enumerate(words):
        if i == th:
            break
        else:
            pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
    pyplot.show()

gofModelPath = "g_of_t.model"
hpModelPath = "h_p.model"
commonModelPath = "common.model"

########## Game of Throne ##########
def trainGot():
    sentences = GofTSentencesCorpus()

    #memory = <numbers of words>*<vectorsize>*4*3
    model = Word2Vec(size = 300,
                        window = 10, #reccomentation of authors for skip-gram approach, 5 for CBOW
                        min_count = 5,
                        workers = 8,
                        sg = 1,
                        negative = 5,
                        sample = 1e-5)

    model.build_vocab(sentences)

    start = time.time()

    model.train(sentences, 
                    total_examples=model.corpus_count, 
                    epochs=100, 
                    report_delay=1,
                    compute_loss = True,
                    callbacks=[Callback()])
    end = time.time()

    print("elapsedtime in seconds :"+ str(end - start))

    model.save(gofModelPath)

def predictGot():
    try:
        model = Word2Vec.load(gofModelPath)
        start = time.time()

        print(model.most_similar(positive=['daenerys'], topn=10))
        
        end = time.time()

        print("elapsedtime in seconds :"+ str(end - start))
    except Exception as e:
        print(e)



################# Harry Potter #############
def trainHp():
    sentences = HPSentencesCorpus()

    #memory = <numbers of words>*<vectorsize>*4*3
    model = Word2Vec(size = 300,
                        window = 10, #reccomentation of authors for skip-gram approach, 5 for CBOW
                        min_count = 5,
                        workers = 8,
                        sg = 1,
                        negative = 5,
                        sample = 1e-5)

    model.build_vocab(sentences)

    start = time.time()

    model.train(sentences, 
                    total_examples=model.corpus_count, 
                    epochs=100, 
                    report_delay=1,
                    compute_loss = True,
                    callbacks=[Callback()])
    end = time.time()

    print("elapsedtime in seconds :"+ str(end - start))

    model.save(hpModelPath)

def predictHp():
    try:
        model = Word2Vec.load(hpModelPath)
        start = time.time()

        print(model.most_similar(positive=['voldemort'], topn=10))
        
        end = time.time()

        print("elapsedtime in seconds :"+ str(end - start))
    except Exception as e:
        print(e)

########### Common model ###############
def trainCommonModel():
    model = Word2Vec.load(gofModelPath)

    sentences = HPSentencesCorpus()

    #model.build_vocab(sentences)

    start = time.time()

    model.train(sentences, 
                    total_examples=model.corpus_count, 
                    epochs=100, 
                    report_delay=1,
                    compute_loss = True,
                    callbacks=[Callback()])
    end = time.time()

    print("elapsedtime in seconds :"+ str(end - start))

    model.save(commonModelPath)

def predictCommon():
    try:
        model = Word2Vec.load(commonModelPath)

        start = time.time()

        print(model.most_similar(positive=['voldemort'], topn=10))
        
        print(model.most_similar(positive=['daenerys'], topn=10))

        end = time.time()

        print("elapsedtime in seconds :"+ str(end - start))
    except Exception as e:
        print(e)


#trainGot()
#trainHp();

#predictGot()
#predictHp()

#trainCommonModel()
#predictCommon()

#visualizeModel(gofModelPath, 300)
visualizeModel(hpModelPath, 300)